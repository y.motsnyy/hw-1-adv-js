"use strict";
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(newName) {
            this._name = newName;
    }

    get age() {
        return this._age;
    }
    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const accountant = new Employee('Anna', 30, 10000)
console.log(accountant); 


const rookieProgrammer = new Programmer('Anton', 25, 10000, 'Javascript');
const expertProgrammer = new Programmer('Taras', 30, 20000, 'Javascript, Python');
const guruProgrammer = new Programmer('Nazar', 35, 40000, 'Javascript, Python, Java');

console.group('Programmers');

console.log(rookieProgrammer);
console.log(rookieProgrammer.name + ': salary - ' + rookieProgrammer.salary);

console.log(expertProgrammer);
console.log(expertProgrammer.name + ': salary - ' + expertProgrammer.salary);

console.log(guruProgrammer);
console.log(guruProgrammer.name + ': salary - ' + guruProgrammer.salary);


